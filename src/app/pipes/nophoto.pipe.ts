import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'nophoto'
})


export class NophotoPipe implements PipeTransform {

  transform(value:string): string {

  	let noimg = "assets/img/tyrone.png";

  	if (value == null) {

  		return noimg;

  	}

    return value;


  }

}
