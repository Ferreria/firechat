import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { Mensaje } from '../interfaces/mensaje.interface';
import { AngularFireAuth,AngularFireAuthProvider } from 'angularfire2/auth';
import { DatePipe } from '@angular/common';
import * as firebase from 'firebase/app';

@Injectable()
export class ChatService {

chats: FirebaseListObservable<any[]>;
lastChats: FirebaseListObservable<any[]>;
usuario:any;
user: Observable<firebase.User>;
firstMessage:any = false;
ultimo:any;
mensaje:Mensaje;

constructor(private db: AngularFireDatabase, public afAuth: AngularFireAuth, private datePipe:DatePipe ) {

  if( localStorage.getItem('usuario') ) {
    this.usuario = JSON.parse(localStorage.getItem('usuario'))
  }else{
    this.user = afAuth.authState;
  }
}

  cargarMensajes(){

    this.chats = this.db.list('/chats',{
      query:{
        limitToLast:20,
        orderByKey: true
      }
    })

    return this.chats

  }

  agregarMensaje( texto:string ) {

    let time:number = Date.now();
    let timeAux:string = this.datePipe.transform(time)


    this.ultimoMensaje().subscribe(
      (data) => {

          this.ultimo = this.datePipe.transform(data[0].hora)

          if(this.ultimo == timeAux){
             this.firstMessage = false;
          }else{
            this.firstMessage = true;
          }

          this.mensaje = {
            nombre:this.usuario.user.displayName,
            mensaje:texto,
            hora: time,
            picture: this.usuario.user.photoURL,
            primerMensaje: this.firstMessage,
            uid: this.usuario.user.uid
          }

      },
      (err) => {
          console.error(err)
      }
  )


    setTimeout ( () => {
          return this.chats.push( this.mensaje );
    },200)
  }

  ultimoMensaje(){

    this.lastChats = this.db.list('/chats',{
      query:{
        limitToLast:1,
        orderBy: 'hora'
      }
    })

    return this.lastChats

  }


  login( proveedor:string ) {

    if( proveedor == 'google' ){
      this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
                      .then( data => {
                            console.log(data),
                            this.usuario = data,
                            localStorage.setItem('usuario',JSON.stringify(data))
                          });
    }else{
      this.afAuth.auth.signInWithPopup(new firebase.auth.TwitterAuthProvider())
                      .then( data => {
                            console.log(data),
                            this.usuario = data,
                            localStorage.setItem('usuario',JSON.stringify(data))
                          });
    }

  }

  logout() {
    console.log('Usuario logged out')
    localStorage.removeItem('usuario');
    this.usuario = null
    this.afAuth.auth.signOut();
  }

}
