import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule} from 'angularfire2/database';
import { AngularFireAuth,AngularFireAuthProvider } from 'angularfire2/auth';
import { environment } from '../environments/environment';

import { ChatService } from './services/chat.service';

import { NophotoPipe } from './pipes/nophoto.pipe';
import { DatePipe } from '@angular/common';

import { AppComponent } from './app.component';
import { ChatComponent } from './components/chat/chat.component';
import { LoginComponent } from './components/login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    ChatComponent,
    LoginComponent,
    NophotoPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule
  ],
  providers: [
    ChatService,
    AngularFireAuth,
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
