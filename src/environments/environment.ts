// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBoEXdjxDiW5wtU0Ut85Fnuh_tFX2VoXmU",
    authDomain: "firechat-ec242.firebaseapp.com",
    databaseURL: "https://firechat-ec242.firebaseio.com",
    projectId: "firechat-ec242",
    storageBucket: "firechat-ec242.appspot.com",
    messagingSenderId: "112929146742"
  }
};
